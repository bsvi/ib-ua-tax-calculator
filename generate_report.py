from converter import converter
from datetime import datetime
import csv

total_tax = 0

def parse_dividends(dividend_data):
    # Example
    # Dividends,Data,USD,2020-12-18,HDV(US46429B6636) Payment in Lieu of Dividend (Ordinary Dividend),5.54

    global total_tax
    for data in dividend_data:
        div_date = datetime.strptime(data[3], '%Y-%m-%d')
        currency = data[2]
        if currency != 'USD':
            raise("Only USD is supported")
        amount = float(data[5])
        uah_amount = amount * converter.get(div_date)

        war_tax = 1.5/100 * uah_amount
        total_tax += war_tax

        print (f"{div_date.strftime('%d.%m.%Y')}: Дивиденды {data[4]} ({amount:.2f}usd={uah_amount:.2f}uah). Налог 1.5% {war_tax:.2f} грн.")

with open('U3775832_20200101_20201218.csv', newline='') as csvfile:
    dividend_data = []
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        if row[0] == 'Dividends' and row[1] == 'Data' and row[2] != 'Total':
            dividend_data.append(row)

    parse_dividends(dividend_data)


print(f'Всего налога {total_tax:.2f} грн')

converter.save()
