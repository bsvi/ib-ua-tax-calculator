import urllib.request
import json
from datetime import date
import pickle

class Converter:
    def __init__(self):
        self.rates = {}
        self.filename = 'rates.pkl'
        self.changed = False
        try:
            with open(self.filename, 'rb') as f:
                self.rates = pickle.load(f)
        except:
            pass

    def save(self):
        if not self.changed:
            return
        with open(self.filename, 'wb') as f:
            pickle.dump(self.rates, f)

    def get(self, d: date) -> float:
        if d not in self.rates:
            date_str = d.strftime("%Y%m%d")
            with urllib.request.urlopen(f"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=USD&date={date_str}&json") as url:
                data = json.loads(url.read().decode())
            self.rates[d] = float(data[0]['rate'])
            self.changed = True

        return self.rates.get(d)

converter = Converter()